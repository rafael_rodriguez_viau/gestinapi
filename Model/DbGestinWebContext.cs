﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace api.Model
{
    public partial class DbGestinWebContext : DbContext
    {
        public DbGestinWebContext()
        {
        }

        public DbGestinWebContext(DbContextOptions<DbGestinWebContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BigTable> BigTables { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Server=PC15\\SQLEXPRESS;Database=DbGestinWeb;Trusted_Connection=True;");
                optionsBuilder.UseSqlServer("Server=DESKTOP-5KDFBNS;Database=DbGestinWeb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BigTable>(entity =>
            {
                entity.ToTable("BigTable");

                entity.Property(e => e.AccreditationType)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Career).HasMaxLength(200);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Subject).HasMaxLength(200);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
