﻿using System;
using System.Collections.Generic;

namespace api.Model
{
    public partial class BigTable
    {
        public int Id { get; set; }
        public int Dni { get; set; }
        public DateTime Date { get; set; }
        public string Career { get; set; } = null!;
        public string Subject { get; set; } = null!;
        public int Grade { get; set; }
        public int SubjectYear { get; set; }
        public string AccreditationType { get; set; } = null!;
    }
}
