using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using api.Model;
using System.Security.AccessControl;
using System.Collections;
using Microsoft.Extensions.Configuration.UserSecrets;
using System.Data;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using api.Migrations;
using System.Xml.Linq;
using System.Drawing;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class BigTableController : ControllerBase
{

    [HttpGet("GetFromGestinMain/{dni}")]
    public IEnumerable getGradesbyDNI(int dni) //solamente de prueba
    {
        using (var dbcontext = new DbGestinWebContext())
        {
            try
            {
                var result = dbcontext.BigTables.Where(x => x.Dni == dni);
                return result;
            }
            catch (SqlException exception) { throw exception; }
        }
    }
    [HttpGet("GetAllFromBigTable")]
    public IEnumerable<BigTable> getAllFromBigTable()
    {
        using (var dbcontext = new DbGestinWebContext())
        {
            try
            {
                var result = dbcontext.BigTables.ToList();
                return result;
            }
            catch (SqlException exception) { throw exception; }
        }
    }
}

