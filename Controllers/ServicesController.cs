using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using api.Model;
using System.Security.AccessControl;
using System.Collections;
using Microsoft.Extensions.Configuration.UserSecrets;
using System.Data;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using api.Migrations;
using System.Xml.Linq;
using System.Drawing;

namespace api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class ServicesController : ControllerBase
{
    SqlConnection connMain = new SqlConnection("server=DESKTOP-5KDFBNS; database=DbGestin; Intergrated Security=true;");
    [HttpGet(Name = "GetAllFromDbGestin")]
    public IEnumerable<BigTable> getAllFromDbGestin() //falta probar
    {
        List<BigTable> resultBigTable = new List<BigTable>();
        using (var cmd = new SqlCommand())
        {
            connMain.Open();
            cmd.Connection = connMain;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText =
             "SELECT gs.AccreditationDate, us.Dni, gs.Grade, sub.Name, car.Name, sub.YearInCareer, gs.AccreditationType FROM [Student] stu"
             + "INNER JOIN [User] us ON us.Id = stu.UserId"
             + "INNER JOIN [Grade] gs ON stu.Id = gs.StudentId"
             + "INNER JOIN [Subject] sub ON sub.Id = gs.SubjectId"
             + "INNER JOIN [Career] car ON car.Id = sub.CareerId";
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var bigmac = new BigTable();
                    bigmac.Date = Convert.ToDateTime(reader["gs.AccreditationDate"]);
                    bigmac.Dni = Convert.ToInt32(reader["us.Dni"]);
                    bigmac.Career = Convert.ToString(reader["sub.Name"]);
                    bigmac.Subject = Convert.ToString(reader["car.Name"]);
                    bigmac.Grade = Convert.ToInt32(reader["gs.Grade"]);
                    bigmac.SubjectYear = Convert.ToInt32(reader["sub.YearInCareer"]);
                    bigmac.AccreditationType = Convert.ToString(reader["gs.AccreditationType"]);
                    resultBigTable.Add(bigmac);
                }
            }
        }
        return resultBigTable;
    }

    public List<BigTable> FilterDuplicateGrades(BigTableRequest body)
    {
        List<BigTable> filteredBigTable = new List<BigTable>();
        foreach(BigTable bt in getAllFromDbGestin())
        {
            if(bt.Grade != body.Grade)
            {
                filteredBigTable.Add(bt);
            }
        }
        return filteredBigTable;
    }

    [HttpPost(Name = "PostBigTable")] //request body && check duplicate grade in subject 
    public IActionResult PostBigTable(BigTableRequest body)
    {
        if(FilterDuplicateGrades(body).Any())
        {
            try
            {
                using (var db = new DbGestinWebContext())
                {
                    db.BigTables.Add(body);
                    db.SaveChanges();
                }
            }
            catch { return StatusCode(500); }
        }
        else
        {
            return NotFound(body);
        }
        return Ok();
    }
}
